package com.bibao.boot.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Customer {
	@Value("Alex")
	private String name;
	private List<String> emails;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getEmails() {
		return emails;
	}
	public void setEmails(List<String> emails) {
		this.emails = emails;
	}
	
	@PostConstruct
	public void initEmails() {
		emails = new ArrayList<>();
		emails.add("alex@gmail.com");
		emails.add("alex@yahoo.com");
	}
}
