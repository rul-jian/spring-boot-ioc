package com.bibao.boot.beans;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import com.bibao.boot.springbootioc.SpringBootIocApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SpringBootIocApplication.class})
public class IoCBootTest {
	@Autowired
	private Employee emp;
	
	@Autowired
	private Student student;
	
	@Autowired
	private Stock stock;
	
	@Autowired
	private Customer customer;
	
	@Autowired
	private Environment env;
	
	@Test
	public void testEmployee() {
		assertEquals(3, emp.getId());
		assertEquals("Robert", emp.getName());
	}

	@Test
	public void testStudent() {
		assertEquals("Jack", student.getName());
		Address address = student.getLocation();
		assertTrue(address instanceof DetailedAddress);
		assertEquals("MD", address.getState());
		assertEquals("Rockville", address.getCity());
		assertEquals("12345", ((DetailedAddress)address).getZipCode());
	}

	@Test
	public void testStock() {
		assertEquals("JPM", stock.getId());
		assertEquals("JP Morgan Chase", stock.getName());
	}
	
	@Test
	public void testEnvironment() {
		assertEquals("bibao", env.getProperty("env.username"));
		assertEquals("abcd1234", env.getProperty("env.password"));
	}
	
	@Test
	public void testCustomer() {
		assertEquals("Alex", customer.getName());
		assertFalse(CollectionUtils.isEmpty(customer.getEmails()));
		List<String> emails = customer.getEmails();
		assertEquals(2, emails.size());
		assertEquals("alex@gmail.com", emails.get(0));
		assertEquals("alex@yahoo.com", emails.get(1));
	}
}
